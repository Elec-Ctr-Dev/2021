### 这里是2021届电控组培训项目归档集中地

---

### 培养计划
<details>
    <summary>点此展开</summary>

```doc
                                            21电控培训规划
▷▮负责人：
    黄文楠、陈露、李彦卿、陈佳依
▷▮参与人员：
    21电控组全体成员，及其他想掌握及巩固电控的成员
▷▮涵盖范围：
    1.电控开发基本概念的认识，理解什么是模拟信号和数字信号，掌握相应的处理方法
    2.编程习惯的培养，良好的代码风格及注释是非常有必要的
    3.Arduino的使用及开发
    4.学习各类传感器的使用方法
    5.掌握硬件电路的搭建、维护、改进方法
    6.学习硬件电路中常用的通信协议，如I2C、SPI、UART、CAN等
    7.熟悉Git版本管理工具，实现源代码管理
    8.掌握硬件抽象编程的思路，实现可重复使用的硬件功能函数库，可能的话，这个阶段需要产出能够供接下来重复调用的硬件库进行存档（推荐使用C++）
    9.基于Stm32 CubeMX，以Keil或MakeFile为开发方式的Stm32单片机核心开发，理解时钟系统、硬件功能复用、中断系统、硬件抽象库（HAL库）、外设使用等等
    10.掌握硬件开发中的断点调试过程，熟练掌握包括Keil、VsCode等开发工具的使用
    11.来完成一个具有挑战性的课题吧
▷▮考核方式：
    1.代码规范的考察
    2.任务task的功能是否实现完成质量
    3.团队协作中展现出的个人魅力
    4.有没有让人眼前一亮的解决方式？
    5.你尽力了吗？
```

</details>

---

### 任务文档

#### 第一周任务文档
<details>
    <summary>点此展开</summary>

1. 完成群内上传Arduino压缩包内的学习任务  
![第一周任务详情](img/第一周任务详情.png)  
每个文件夹内都有单独的任务介绍，其中，18、20、21、22、24因为硬件器材以及实用性的原因取消，当然感兴趣可以自行理解。  
Arduino是一种非常简单的单片机，在比赛中常用来检测传感器、快速验证功能，对于新拿到的元器件，如果不会使用的话，去查一查与之相关的Arduino历程是一个不错的选择，作为合格的电控人，需要具备一定的元器件认识量，同时对于未知的元器件，快速掌握其功能及应用也是很重要的能力。  
1. 对于学有余力的同学，可以来尝试一下附加任务：
还记得实验室初版招募指南的附加任务Git的使用吗？没错，这个附加任务我认为非常值得应用，对于需要敲代码的人来说，这实在是一个必备技能，虽然这次是附加任务，但是之后的所有任务的提交都会与之挂钩。    

##### 这次的附加任务内容是：  
1. 创建一个GitLab的账号，并设置自己的语言偏好为中文（你英语好不改也行）
2. 生成ssh密钥并添加到自己的Git账户
3. 在计算机上安装Git，并尝试拉取这个仓库->https://gitlab.com/Elec-Ctr-Dev/Guide

</details>

#### 第二周任务文档
<details>
    <summary>点此展开</summary>

##### 任务要求
做一辆小车，要求实现循迹、避障和蓝牙通信功能。循迹路线如下图所示：  
![循迹路线](img/第二周循迹路线.png)
&emsp;
&emsp;
&emsp;  

1、从出发点出发到终点结束，不偏离循迹路线（车身投影始终在黑线上）；
&emsp;  

2、从指定位置出发，并完成一次拐弯。 
&emsp;

3、小车启动后，通过蓝牙发送任意信号给接收器。接收器为手机或电脑二选一（手机蓝牙调试器在群文件及项目归档中，目前只有安卓版本，没有ios版本)。
&emsp;  
&emsp;  


##### 评分标准
1、任务要求1中，偏离一次扣5分，总分20分。 
&emsp;  

2、任务要求2中，三次机会，完成2次得10分，一次得5分。 
&emsp;

3、任务要求3中，接收器为手机得10分，电脑得20分。

##### 任务提交
拍摄任务视频，命名为“任务周数+姓名”。拍摄时，需要在评分要求的关键部分给特写；由于拍摄问题导致无法评分或未按要求提交，则视为未交。

</details>

#### 第三周任务文档
<details>
    <summary>点此展开</summary>

##### 任务要求
1、安装KEIL5  
2、使用寄存器点亮LED  
3、使用HAL库点亮LED  
4、使用标准库点亮LED  

**由于ST公司不再更新标准库，以及STM32H7系列的芯片不提供标准库，HAL库将变成主流，因此我们不要求必须掌握标准库。标准库的任务将成为附加任务，学有余力的同学可以学习，这也会列入评判标准，以后将不再特别说明。**

##### 推荐学习教程
[KELI5 安装教程](https://blog.csdn.net/qq_42748213/article/details/90485750)  
[HAL库教程](https://www.bilibili.com/video/BV18X4y1M763?p=7)  
[标准库教程](https://www.bilibili.com/video/BV1Rx411R75t?p=10)

##### 提交要求
将整个工程文件打包压缩，并命名为 "任务周数+姓名"。若未按要求提交，则视为未交。

</details>

#### 第四周任务文档
<details>
    <summary>点击展开</summary>

考虑到期末考试在即，本周起任务会相对简单一些，对硬件要求不会那么苛刻。

##### 任务要求
1. 配置`VsCode`下Stm32开发环境，实现与`Keil`联动或`Makefile`方式开发，但主要是学习`Keil Assistant`插件下的工作区的使用，`Makefile`开发方式比较难啃所以不做太多要求。
2. 使用`Stm32CubeMX`生成对应芯片的HAL库工程，工具链为`MDK-ARM`或`Makefile`都可以，但后者有一定门槛(蛮高的，不过会了之后很爽)。
3. 要求正确配置时钟树，使得芯片时序精准，具体需要自行了解所用开发板的晶振型号(对于`f103xx`系列通常是8Mhz)。
4. 在工程目录(不论`Keil`或`Makefile`开发方式)下新建User文件夹，并在下方新建Inc和Src两个文件夹，前者用于放置`.h/.hpp`头文件，后者用于放置`.c/.cpp`源码文件。
5. 于上述`User`目录下指定位置创建`led.h`与`led.c`，实现1s一次的LED翻转，要求引脚**不能使用**`Stm32CubeMX`图形化配置，需要手动调用HAL库函数开启。
##### 任务提交
1、实现LED准确翻转的视频，视频长度为5s即可。  
2、整个工作区文件夹的压缩包，重命名为`姓名+第四周任务提交`。  

需要将两个文件上传至`GitLab`第四周提交目录下，分别放到视频和工作区提交**两个**文件夹中  
[提交目录指路](https://gitlab.com/Elec-Ctr-Dev/2021/-/tree/master/%E7%AC%AC%E5%9B%9B%E5%91%A8%E4%BB%BB%E5%8A%A1%E6%8F%90%E4%BA%A4)  

尽量使用Git推送的方式上传，即`clone`本仓库，或在本地初始化空仓库后，设置本仓库为远程存储库，提交你需要上传的文件，并其`push`上传。

~~当然使用网页端上传也可以，不过这样拿不到附加分咯~~


##### 评分标准
1. 工作区是否按要求配置，是否干净整洁
2. 任务要求是否完成
3. 功能是否实现
4. 结合第二周任务提交情况总结指出的问题
5. ~~当然是附加的内容咯~~

##### 提示
可以先使用图形化配置，跟随main函数看看发生了什么，再手动完成要求`5.`

</details>

#### 第五周任务文档
<details>
    <summary>点击展开</summary>

##### 任务要求
经过上一周的学习，大家已经初步掌握了Git仓库的工作流程，并且初步了解了时钟系统的构成，尝试了1s翻转LED灯实验，本周是对上一周学习内容的进一步检验，并且实验室采购的Stm32核心板已经到货，请大家**务必使用统一的核心板**完成实验内容。  

本周实验需要完成的任务是：  
1. 复习时钟系统，正确配置实验室采购核心板的时钟系统
2. 参照上周的工程配置方式，于`User/Inc`及`User/Src`分别建立`Uart1.h`和`Uart1.c`;
3. 于上述新建文件内实现核心板与电脑的串口通讯，向电脑输出`"Hello World"`。  

##### 注意事项
1. Stm32串口通讯与`Arduino`不同，并不能简单通过USB接口直接向电脑输出信息(不是不能，而是USB驱动及其他细节需要自己配置)，需要通过USB转串口模块(TTL),或者`CMSIS-DAP`调试器实现，且接线细节需要注意。
2. 由于串口`Uart`是异步时钟通信，时钟配置准确与否直接决定了电脑能否正确收到信息，如果收到的是乱码，那一定是时钟系统配置出现了问题。
3. 电脑上接收串口通讯的软件可以使用`Arduino`自带串口通讯软件
##### 任务提交
1. 整个工作区打包，并重命名为`"姓名+第五周任务提交"`,上传至`"第五周任务提交\工作区提交"`内，**务必确认无误且不要忘记发起和并请求**。
2. 实现串口通信的视频,视频需要包含板子程序编译上传的过程，以及电脑接收串口通讯的画面，要求**一镜到底**，视频提交至`"第五周任务提交\视频提交"`内。  
 
上周仍然有同志不按照要求来提交文件，这次开始任务提交是否规范也会算一点点分咯。

</details>

#### 第六周任务文档
<details>
    <summary>点击展开</summary>

##### 任务要求
由于本周为考试周，本周任务将减小难度。下一周任务将深入HAL库，利用HAL库完成UART中断处理，难度较大，因此本周任务让你们浅尝一下HAL库中断处理,下周利用HAL库造火箭。
1. 学习外部中断，理解中断处理过程。
2. 使用外部中断，完成按键点灯（长按关闭，短按一次常亮，短按两次闪烁一次）
3. 了解HAL_GPIO_EXTI_IRQHandler()与回调函数之间的关系。  
[用法提示](https://blog.csdn.net/zrb2753/article/details/105903847)

##### 任务提交
1. 整个工作区打包，并重命名为`"姓名+第六周任务提交"`,上传至`"第六周任务提交\工作区提交"`内，**务必确认无误且不要忘记发起合并请求**。
2. 实现按键点灯长按、短按一次、短按两次的视频,视频需要包含板子程序编译上传的过程，以及电脑下载程序的画面，要求**一镜到底**，视频提交至`"第六周任务提交\视频提交"`内。  
3. **自本次提交起视频没有编译过程的提交一律扣分**
 
</details>

</details>

#### 第七周任务文档
<details>
    <summary>点击展开</summary>
中断是学习STM32的重点和难点，没有中断的单片机是没有灵魂的。经过上周的学习大家基本上还没有掌握中断的含义，希望大家再接再厉，我们本周继续学习中断。  

##### 任务要求
  
画出外部中断处理过程的流程图，具体要求体现:  
- 中断产生前后中断标志位的变化  
- 中断是否使能带来的影响  
- `NVIC`在中断产生到处理这一过程中产生的作用  
- `EXTIx_IRQHandler`、`HAL_GPIO_EXTI_IRQHandler`、  `HAL_GPIO_EXTI_Callback`的包含关系
- 使用`__HAL_GPIO_EXTI_GET_IT()`和`__HAL_GPIO_EXTI_CLEAR_IT()`后中断标志位是否变化
&emsp;  

[免费流程图制作网站](https://www.iodraw.com/diagram/)
##### 任务提交
整个工作区打包，并重命名为`"姓名+第七周任务提交"`,上传至`"第七周任务提交\工作区提交"`内，**务必确认无误且不要忘记发起合并请求**。


</details>

#### 第八周任务文档
<details>
    <summary>点击展开</summary>

##### 任务要求
定时器的使用是单片机实现复杂功能必不可少的资源之一，使用定时器能够实现许多控制信号，本次任务我们将学习如何使用定时器输出特定的PWM波，并借此控制一些基础的元器件。  

具体任务内容如下：  
① 理解STM32定时器的使用，并通过定时器输出PWM信号实现呼吸灯  
② 理解舵机控制信号原理，并通过定时器产生对应的PWM信号控制舵机旋转  


##### 相关资料
[PWM信号简述](https://blog.csdn.net/as480133937/article/details/103439546)  
[STM32 HAL舵机控制](https://blog.csdn.net/qq153471503/article/details/102987649)  


##### 任务提交
1. 整个工作区打包，并重命名为`"姓名+第八周任务提交"`,上传至`"第八周任务提交\工作区提交"`内。
2. 实现呼吸灯及舵机旋转的视频，提交到`"第八周任务提交\视频提交"`内。  

**务必确认无误且不要忘记发起合并请求**。


</details>
